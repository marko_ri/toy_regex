use toy_regex::{matcher, parser};

#[test]
fn test_easy() {
    let regex = b"a?bc";
    let states = parser::parse(regex);
    let states = states.first().unwrap();
    let input = b"abc";
    let actual = matcher::matches(states, input);
    let expected = (true, 3);
    assert_eq!(expected, actual);

    let input = b"bc";
    let actual = matcher::matches(states, input);
    let expected = (true, 2);
    assert_eq!(expected, actual);

    let input = b"zbc";
    let actual = matcher::matches(states, input);
    let expected = (false, 0);
    assert_eq!(expected, actual);
}

#[test]
fn test_more_complex() {
    let regex = b"a(b.)*cd";
    let states = parser::parse(regex);
    let states = states.first().unwrap();
    let input = b"ab!b$cd";
    let actual = matcher::matches(states, input);
    let expected = (true, 7);
    assert_eq!(expected, actual);

    let input = b"ac";
    let actual = matcher::matches(states, input);
    let expected = (false, 2);
    assert_eq!(expected, actual);
}

#[test]
fn test_backtracking() {
    let regex = b"a.*c";
    let states = parser::parse(regex);
    let states = states.first().unwrap();
    let input = b"acc";
    let actual = matcher::matches(states, input);
    let expected = (true, 3);
    assert_eq!(expected, actual);
}
