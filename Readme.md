
Toy regular expression engine in Rust, based on the video: [How Do Regular Expressions Really Work](https://www.youtube.com/watch?v=u01jb8YN2Lw).
