// Only a small subset of regex features are used:
//   .  wildcard
//   \x escaped literals (for capturing dots or parentheses)
//   () grouping parentheses
//   +  greedy one or more quantifier
//   *  greedy zero or more quantifier
//   ?  zero or one quantifier
// Everything else is considered to be a literal
// ASCII only

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct Element {
    pub kind: ElementKind,
    pub quantifier: ElementQuantifier,
    pub states: Vec<Element>,
    pub value: Option<u8>,
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub enum ElementKind {
    Wildcard,
    Literal,
    GroupElement,
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub enum ElementQuantifier {
    ExactlyOne,
    ZeroOrOne,
    ZeroOrMore,
}

pub fn parse(re: &[u8]) -> Vec<Vec<Element>> {
    let mut stack: Vec<Vec<Element>> = vec![vec![]];
    let mut iter = re.iter().enumerate();

    while let Some((i, next)) = iter.next() {
        match *next {
            b'.' => {
                stack.last_mut().unwrap().push(Element {
                    kind: ElementKind::Wildcard,
                    quantifier: ElementQuantifier::ExactlyOne,
                    states: vec![],
                    value: None,
                });
            }
            b'\\' => {
                let Some((_, value)) = iter.next() else {
                    panic!("Bad escape character at index {i}");
                };
                stack.last_mut().unwrap().push(Element {
                    kind: ElementKind::Literal,
                    quantifier: ElementQuantifier::ExactlyOne,
                    value: Some(*value),
                    states: vec![],
                });
            }
            b'(' => {
                stack.push(vec![]);
            }
            b')' => {
                if stack.len() <= 1 {
                    panic!("No group to close at index {i}");
                }
                let states = stack.pop().unwrap();
                stack.last_mut().unwrap().push(Element {
                    kind: ElementKind::GroupElement,
                    quantifier: ElementQuantifier::ExactlyOne,
                    states,
                    value: None,
                });
            }
            b'?' => {
                let last_element = stack.last_mut().and_then(|v| v.last_mut()).unwrap();
                if last_element.quantifier != ElementQuantifier::ExactlyOne {
                    panic!("Quantifier must follow an unquantified element or group");
                };
                last_element.quantifier = ElementQuantifier::ZeroOrOne;
            }
            b'*' => {
                let last_element = stack.last_mut().and_then(|v| v.last_mut()).unwrap();
                if last_element.quantifier != ElementQuantifier::ExactlyOne {
                    panic!("Quantifier must follow an unquantified element or group");
                };
                last_element.quantifier = ElementQuantifier::ZeroOrMore;
            }
            b'+' => {
                let last_element = stack.last_mut().and_then(|v| v.last_mut()).unwrap();
                if last_element.quantifier != ElementQuantifier::ExactlyOne {
                    panic!("Quantifier must follow an unquantified element or group");
                };
                // Split this into two operations:
                // 1. exactlyOne of the previous quantified element
                // 2. zeroOrMore of the previous quantified element
                let zero_or_more_copy = Element {
                    kind: last_element.kind.clone(),
                    states: last_element.states.clone(),
                    quantifier: ElementQuantifier::ZeroOrMore,
                    value: last_element.value,
                };
                stack.last_mut().unwrap().push(zero_or_more_copy);
            }
            value => {
                stack.last_mut().unwrap().push(Element {
                    kind: ElementKind::Literal,
                    quantifier: ElementQuantifier::ExactlyOne,
                    value: Some(value),
                    states: vec![],
                });
            }
        }
    }

    if stack.len() != 1 {
        panic!("Unmatched groups in regular expression");
    }

    stack
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    #[should_panic(expected = "Bad escape character at index 3")]
    fn test_bad_escape_char() {
        let re = b"abc\\";
        parse(re);
    }

    #[test]
    fn test_good_escape_char() {
        let re = b"a\\?";
        let actual = parse(re);
        let expected = vec![vec![
            Element {
                kind: ElementKind::Literal,
                quantifier: ElementQuantifier::ExactlyOne,
                states: vec![],
                value: Some(b'a'),
            },
            Element {
                kind: ElementKind::Literal,
                quantifier: ElementQuantifier::ExactlyOne,
                states: vec![],
                value: Some(b'?'),
            },
        ]];
        assert_eq!(expected, actual);
    }

    #[test]
    #[should_panic(expected = "Unmatched groups in regular expression")]
    fn test_missing_closing_bracket() {
        let re = b"a(b";
        parse(re);
    }

    #[test]
    #[should_panic(expected = "No group to close at index 2")]
    fn test_missing_opening_bracket() {
        let re = b"ab)";
        parse(re);
    }

    #[test]
    fn test_good_parse() {
        let re = b"a?(b.*c)+d";
        let actual = parse(re);
        let expected = vec![vec![
            Element {
                kind: ElementKind::Literal,
                quantifier: ElementQuantifier::ZeroOrOne,
                states: vec![],
                value: Some(b'a'),
            },
            Element {
                kind: ElementKind::GroupElement,
                quantifier: ElementQuantifier::ExactlyOne,
                states: vec![
                    Element {
                        kind: ElementKind::Literal,
                        quantifier: ElementQuantifier::ExactlyOne,
                        states: vec![],
                        value: Some(b'b'),
                    },
                    Element {
                        kind: ElementKind::Wildcard,
                        quantifier: ElementQuantifier::ZeroOrMore,
                        states: vec![],
                        value: None,
                    },
                    Element {
                        kind: ElementKind::Literal,
                        quantifier: ElementQuantifier::ExactlyOne,
                        states: vec![],
                        value: Some(b'c'),
                    },
                ],
                value: None,
            },
            Element {
                kind: ElementKind::GroupElement,
                quantifier: ElementQuantifier::ZeroOrMore,
                states: vec![
                    Element {
                        kind: ElementKind::Literal,
                        quantifier: ElementQuantifier::ExactlyOne,
                        states: vec![],
                        value: Some(b'b'),
                    },
                    Element {
                        kind: ElementKind::Wildcard,
                        quantifier: ElementQuantifier::ZeroOrMore,
                        states: vec![],
                        value: None,
                    },
                    Element {
                        kind: ElementKind::Literal,
                        quantifier: ElementQuantifier::ExactlyOne,
                        states: vec![],
                        value: Some(b'c'),
                    },
                ],
                value: None,
            },
            Element {
                kind: ElementKind::Literal,
                quantifier: ElementQuantifier::ExactlyOne,
                states: vec![],
                value: Some(b'd'),
            },
        ]];
        assert_eq!(expected, actual);
    }
}
