use super::parser::*;

fn matches_at_index(state: &Element, input: &[u8], input_idx: usize) -> (bool, usize) {
    if input_idx >= input.len() {
        return (false, 0);
    }
    match state.kind {
        ElementKind::Wildcard => (true, 1),
        ElementKind::GroupElement => {
            // check if all of its substates match, from this point forward
            matches(&state.states, &input[input_idx..])
        }
        ElementKind::Literal => {
            if state.value == Some(input[input_idx]) {
                (true, 1)
            } else {
                (false, 0)
            }
        }
    }
}

struct Backtrack<'a> {
    is_backtrackable: bool,
    state: &'a Element,
    consumptions: Vec<usize>,
}

fn backtrack<'a>(
    current_state: &'a Element,
    states_rev: &mut Vec<&'a Element>,
    input_idx: &mut usize,
    backtrack_stack: &mut Vec<Backtrack<'a>>,
) -> bool {
    states_rev.push(current_state);
    let mut could_backtrack = false;

    while let Some(mut bt) = backtrack_stack.pop() {
        if bt.is_backtrackable {
            if bt.consumptions.is_empty() {
                // the same as being unbacktrackable
                states_rev.push(bt.state);
                continue;
            }
            // if we have consumed any characters
            let n = bt.consumptions.pop().unwrap();
            // move our index backwards in the input string
            *input_idx -= n;

            // there could still be more to give back from the state...
            backtrack_stack.push(bt);
            could_backtrack = true;
            break;
        } else {
            // state is unbacktrackable
            states_rev.push(bt.state);
            for consumption in bt.consumptions {
                // move idx backwards as many chars as this state consumed
                *input_idx -= consumption;
            }
        }
    }

    could_backtrack
}

pub fn matches(states: &[Element], input: &[u8]) -> (bool, usize) {
    // we are only pop-ing/push-ing on one side, so no need for VecDeque (?)
    let mut states_rev: Vec<&Element> = states.iter().rev().collect();
    let mut input_idx = 0;
    let mut backtrack_stack: Vec<Backtrack> = Vec::new();

    while let Some(current_state) = states_rev.pop() {
        match current_state.quantifier {
            ElementQuantifier::ExactlyOne => {
                let (is_match, consumed) = matches_at_index(current_state, input, input_idx);

                if !is_match {
                    let idx_before_backtracking = input_idx;
                    // try to backtrack here
                    let could_backtrack = backtrack(
                        current_state,
                        &mut states_rev,
                        &mut input_idx,
                        &mut backtrack_stack,
                    );
                    if !could_backtrack {
                        return (false, idx_before_backtracking);
                    }
                } else {
                    backtrack_stack.push(Backtrack {
                        is_backtrackable: false,
                        state: current_state,
                        consumptions: vec![consumed],
                    });
                    input_idx += consumed;
                }
            }
            ElementQuantifier::ZeroOrOne => {
                if input_idx >= input.len() {
                    backtrack_stack.push(Backtrack {
                        is_backtrackable: false,
                        state: current_state,
                        consumptions: vec![0],
                    });
                } else {
                    let (is_match, consumed) = matches_at_index(current_state, input, input_idx);
                    input_idx += consumed;
                    backtrack_stack.push(Backtrack {
                        is_backtrackable: is_match && consumed > 0,
                        state: current_state,
                        consumptions: vec![consumed],
                    });
                }
            }
            ElementQuantifier::ZeroOrMore => {
                let mut backtrack_state = Backtrack {
                    is_backtrackable: true,
                    state: current_state,
                    consumptions: vec![],
                };

                loop {
                    // we reached the end of input
                    if input_idx >= input.len() {
                        if backtrack_state.consumptions.is_empty() {
                            backtrack_state.is_backtrackable = false;
                            backtrack_state.consumptions.push(0);
                        }
                        backtrack_stack.push(backtrack_state);
                        break;
                    }

                    let (is_match, consumed) = matches_at_index(current_state, input, input_idx);
                    if !is_match || consumed == 0 {
                        if backtrack_state.consumptions.is_empty() {
                            backtrack_state.is_backtrackable = false;
                            backtrack_state.consumptions.push(0);
                        }
                        backtrack_stack.push(backtrack_state);
                        break;
                    }

                    backtrack_state.consumptions.push(consumed);
                    input_idx += consumed;
                }
            }
        }
    }

    (true, input_idx)
}
